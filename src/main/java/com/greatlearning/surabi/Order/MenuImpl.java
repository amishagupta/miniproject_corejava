package com.greatlearning.surabi.Order;

import java.util.HashMap;
import java.util.Map;

import user.User;

public class MenuImpl {
	private Map <Integer,menu> menu = new HashMap<>(); 
	
	public MenuImpl()
	{
		menu.put(1,new menu(1,"Rajma Chaval",2,150));
		menu.put(2,new menu(2,"momos",2,190));
		menu.put(3,new menu(3,"Red thai Curry",2,180));
		menu.put(4,new menu(4,"chaap",2,190));
		menu.put(5,new menu(5,"chilly patato",1,250));
	}
	public void displayMenu() {
		System.out.println("Todays Menu :-");
		menu.forEach((k,v) -> System.out.println(v));
	}
	public menu getOrderItem(int itemId) {
		return menu.get(itemId);
	}

}
