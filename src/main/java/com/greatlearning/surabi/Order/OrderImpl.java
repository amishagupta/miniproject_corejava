package com.greatlearning.surabi.Order;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class OrderImpl {
	private Map <Integer,Order>  orders= new HashMap<>();
	public OrderImpl() {
		// TODO Auto-generated constructor stub
		ArrayList <menu> items = new ArrayList<>();
		items.add(new menu(1,"Rajma Chaval",2,150));
		items.add(new menu(1,"Rajma Chaval",2,150));
		Order newOrder = new Order();
		newOrder.createOrder("Ram", items);
		orders.put(1,newOrder);
		newOrder = new Order();
		newOrder.createOrder("Ravi", items);
		orders.put(2,newOrder);
	}
	public void addOrder(Order newOrder) {
		orders.put(orders.size()+1,newOrder);
	}
	public void getAllOrders() {
		orders.forEach((k,v) -> System.out.println(v));
	}
	public void getOrdersbyDate() {
		orders.forEach((k,v) -> {
			if((v.getDoo().getDate() == new Date().getDate())&&(v.getDoo().getMonth() == new Date().getMonth())) {
				System.out.println(v);
			}
		});
		
	}
	public void getOrderByMonth() {
		orders.forEach((k,v) -> {
			if(v.getDoo().getMonth() == new Date().getMonth()) {
				System.out.println(v);
			}
		});
		
	}

}
