package com.greatlearning.surabi.Order;

public class menu {
	private int dishId;
	private String dishName;
	private int dishServing;
	private float dishPrice;
	
	menu(int dishId,String dishName,int dishServing,float dishPrice){
		setDishId(dishId);
		setDishName(dishName);
		setDishServing(dishServing);
		setDishPrice(dishPrice);
	}
	
	public int getDishId() {
		return dishId;
	}
	public void setDishId(int dishId) {
		this.dishId = dishId;
	}
	public String getDishName() {
		return dishName;
	}
	public void setDishName(String dishName) {
		this.dishName = dishName;
	}
	public int getDishServing() {
		return dishServing;
	}
	public void setDishServing(int dishServing) {
		this.dishServing = dishServing;
	}
	public float getDishPrice() {
		return dishPrice;
	}
	public void setDishPrice(float dishPrice) {
		this.dishPrice = dishPrice;
	}
	
	@Override
	public String toString() {
		return dishId+" "+ dishName+" "+dishServing+" "+dishPrice;
	}
	
	

}
