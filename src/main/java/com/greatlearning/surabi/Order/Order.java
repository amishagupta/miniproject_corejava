package com.greatlearning.surabi.Order;

import java.util.ArrayList;
import java.util.Date;

public class Order {
	private int OrderId;
	private String userEmail;
	private Date doo;
	private ArrayList<menu> items;
	private float totalBill;
	
//	Order(String userEmail,menu items[]){
//		setUserEmail(userEmail);
//		setDoo(new Date());
//		setItems(items);
//		
//	}
	public int getOrderId() {
		return OrderId;
	}
	public void setOrderId(int orderId) {
		OrderId = orderId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public Date getDoo() {
		return doo;
	}
	public void setDoo(Date doo) {
		this.doo = new Date();
	}
	public ArrayList<menu> getItems() {
		return items;
	}
	public void setItems(ArrayList<menu> items) {
		this.items = items;
	}
	public float getTotalBill() {
		return totalBill;
	}
	public void setTotalBill(float totalBill) {
		this.totalBill += totalBill;
	}
	
	public void createOrder(String userEmail,ArrayList<menu> items) {
		setUserEmail(userEmail);
		setDoo(new Date());
		setItems(items);
		items.forEach((e) -> setTotalBill(e.getDishPrice()));
	}
	void updateOrder(menu item) {
		items.add(item);
	}
	public void DisplayBill() {
		System.out.println("Thanks Mr "+getUserEmail()+" for dinning in with Surabi");
		System.out.println("Items You have selectec:- ");
		items.forEach((e) -> System.out.println(e));
		System.out.println("You total bill will be:-  "+getTotalBill());
		
		
	}
	
	@Override
	public String toString() {
		return OrderId+"\nUserName:- "+ userEmail+"\nItems:- "+items+"\nTotal:- "+totalBill+"\nDate "+doo;
	}
	
	

}
