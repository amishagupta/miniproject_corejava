package com.greatlearning.surabi;

import user.AuthUser;
import user.User;

import java.util.ArrayList;
import java.util.Scanner;

import com.greatlearning.surabi.Order.MenuImpl;
import com.greatlearning.surabi.Order.Order;
import com.greatlearning.surabi.Order.OrderImpl;
import com.greatlearning.surabi.Order.menu;

public class App {
  public static void main(String[] args) {
	  
	  
	  ArrayList<menu> items = new ArrayList<>();
	  MenuImpl menuImpl = new MenuImpl();
	  Order order = new Order();
	  OrderImpl orderImpl = new OrderImpl();
	  User user;
	  do {
		// Using Scanner for Getting Input from User
	      Scanner in = new Scanner(System.in);
	      System.out.println("Welcome to Surabi Restaurants ");
	      System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	      
	      System.out.println("Please enter your credentials");
	      System.out.println("Email id =");
	      String email = in.nextLine();
     
	      System.out.println("Password = ");
	      String password = in.nextLine();
	      
	      System.out.println("please enter A if you are admin and U if you are user");
	      String role = in.nextLine();
	      
      
	      AuthUser newUser = new AuthUser();
	      user = newUser.loginUser(email,password,role);
	      if(user!=null) {
	    	  if(role.equals("A")) {
	    		  System.out.println("Welcome Admin");
	    		  System.out.println("Press 1 to see all the bills for today");
	    		  System.out.println("Press 2 to see all the bills for this month");
	    		  System.out.println("Press 3 to see all the bills");
	    		  int adminMenu = in.nextInt();
	    		  switch(adminMenu) {
	    		  case 1:
	    			  orderImpl.getOrdersbyDate();
	    			  break;
	    		  case 2:
	    			  orderImpl.getOrderByMonth();
	    			  break;
	    		  case 3:
	    		  	  orderImpl.getAllOrders();
	    		  	  break;
	    		default:
	    			System.out.println("Worng Input");
	    			break;
	    		  }
	    		  
	    	  }
	    	  else {
	    	  System.out.println("Welcome Mr "+user.getUserName());
	    	  int nextKey = 0;
	    	  do 
	    	  {
	    		  menuImpl.displayMenu();
	    		  System.out.println("Enter the Menu Item code");
	    		  int menuKey = in.nextInt();
	    		  items.add(menuImpl.getOrderItem(menuKey));
	    		  System.out.println("Press 0 to show the bill");
	    		  System.out.println("Press 1 to order more");
	    		  nextKey = in.nextInt();
		  
	    	  }while (nextKey == 1);
	    	  if(nextKey == 0)
	    	  {
	    		  order.createOrder(user.getUserName(), items);
	    		  orderImpl.addOrder(order);
	    		  order.DisplayBill();
	    		  
	    	  }
	    	}
	      }
	      else 
	      {
	    	  System.out.println("user is invalid please enter the credentials again");
	      }
	  }while(user == null);
  }
}
