package user;

import java.util.HashMap;
import java.util.Map;

public class AuthUser{
	
	private Map <Integer,User> usersMap = new HashMap<>(); 
	private Map <Integer,User> adminMap = new HashMap<>();
	
	public AuthUser(){
	usersMap.put(1,new User("Ram","ram@gmail.com","ram"));
	usersMap.put(2, new User("Ajay","ajay@gmail.com","ajay"));
	usersMap.put(3,new User("Deepak","deepak@gmail.com","deepak"));
	
	adminMap.put(1,new User("admin1","admin1@gmail.com","admin1"));
	adminMap.put(2, new User("admin2","admin2@gmail.com","admin2"));
	adminMap.put(3,new User("admin3","admin3@gmail.com","admin3"));
	}	
	
	public User loginUser(String userEmail,String userPassword,String role) {
		User data;
		User result =null;
		System.out.println(role);
		if(role.equals("U")) {
			
			for (Map.Entry<Integer, User> entry : usersMap.entrySet()) {
		        if((userEmail.equals(entry.getValue().getUserEmail()))&& (userPassword.equals(entry.getValue().getUserPassword())))
		            result = entry.getValue();
			}
			
		}
		else if(role.equals("A")) {
			for (Map.Entry<Integer, User> entry : adminMap.entrySet()) {
		        if((userEmail.equals(entry.getValue().getUserEmail()))&& (userPassword.equals(entry.getValue().getUserPassword())))
		            result = entry.getValue();
			}
		}
		//System.out.println(result);
		return result;
		
	}
	public void logoutUser() {
		// TODO Auto-generated method stub
		System.out.println("Thanks for Dining in with us");
	}
	

}
