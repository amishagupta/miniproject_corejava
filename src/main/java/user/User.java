package user;

public class  User {
	
	String userEmail = "";
	String userPassword = "";
	String userName = "";
	
	User(String userName,String userEmail,String userPassword){
		
		setUserEmail(userEmail);
		setUserPassword(userPassword);
		setUserName(userName);
	}
	
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return userEmail+" "+userPassword;
	}
}
