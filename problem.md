# Problem Statement

Surabi is a chain of restaurants. Till this time there billing system was manual but due to COVID 19 they wanted to have a Billing System to provide better service to their customers.

## Use Stories
1. As a user I should be able to login and logout from the application.
2. As a user I should be able to see all the items available along with price.
3. As a user I should be able to select the item by entering their sequence number.
4. As a user I should be able to select more than one item.
5. As a user I should be able to see my final bill on the screen.
6. As an admin I should be able to login and logout from the application
7. As an admin I should be able to see all the bills getting generated today.
8. As an admin I should be able to see the total sale from this month.

## Instructions: -

1.	In order to make your application efficient please use new Thread as soon as a user logged in and the threads properly closed as soon as a user logout. 
2.	A user should get the proper see the error messages if some input is not correct.
3.	The errors should be handled using exception handling.
4.	To manage the data into the application please user proper collections.
5.	Use proper interfaces and classes combo to create the application.

# Optional Features: -
1.	Login and Logout features should be managed using files.
2.	Bills info should be stored into files.
